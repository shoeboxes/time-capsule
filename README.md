## Running

```
docker volume create --name=time-capsule
docker-compose up -d
```

## Time machine

```
defaults write com.apple.systempreferences TMShowUnsupportedNetworkVolumes 1  
```
