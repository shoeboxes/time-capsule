FROM armhf/ubuntu:14.04

ENV DEBIAN_FRONTEND noninteractive

RUN sudo apt-get update && \
    sudo apt-get install -y netatalk avahi-daemon supervisor && \
    rm -rf /var/lib/apt/lists/* && \
    mkdir -p /var/log/supervisor

ADD supervisord.conf /etc/supervisor/conf.d/supervisord.conf
ADD run.sh /run.sh

EXPOSE 548

CMD ["/bin/sh", "/run.sh"]
